const GOOGLE_SUGGEST_URL = "http://google.com/complete/search?output=toolbar&q=";

class SuggestionProvider {
	constructor() {
		// Properties
		
		// Functions
		this.httpGet = this.httpGet.bind(this);
		this.getSuggestions = this.getSuggestions.bind(this);
		this.processSuggestions = this.processSuggestions.bind(this);

		// Init
	}

	/** 
	*@param string   url 		the API endpoint with parameters included
	*@param function callback	function to pass response async
	*/
	httpGet(url, callback) {
		var xmlHttp = new XMLHttpRequest();

   		xmlHttp.onreadystatechange = () => { 
        	if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            	callback(xmlHttp);
    	};

    	xmlHttp.open("GET", url, true); // true for asynchronous 
    	xmlHttp.send(null);
	}

	/** 
	*@param  AutosuggestControl AutosuggestControl	calling function
	*@param  string   			q 		    		search string
	*	
	*@return string[] 			suggestions 		array of suggestions
	*/
	getSuggestions(AutosuggestControl, q) {
		q = q ? q : " "; // Google suggest returns errors for ""

		this.httpGet(encodeURI(GOOGLE_SUGGEST_URL + q), xmlHttp => {
			const suggestions = this.processSuggestions(xmlHttp.responseXML)
			AutosuggestControl.fillSuggestions(suggestions);
		});
	}

	/**
	*@param  string 	responseXML		xml reponse provided by google suggest API
	*
	*@return string[]	suggestions 	array of suggestions
	*/
	processSuggestions(responseXML) {
		var suggestions = [];
		const suggestionsXML = responseXML.getElementsByTagName('CompleteSuggestion');

		for (let i = 0; i < suggestionsXML.length; i++)
			suggestions.push(suggestionsXML[i].childNodes[0].getAttribute('data'));
		
		return suggestions;
	}
}
/** 
*@param HTMLInputElement 	textbox
*@param SuggestionProvider	provider
*/
class AutoSuggestControl {
	constructor(textbox, provider) {
		// Props
		this.textbox = textbox;
		this.provider = provider;
		this.layer = document.getElementById('suggestions');

		// Functions
		this.init = this.init.bind(this);
		this.fillSuggestions = this.fillSuggestions.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);

		// Init
		this.init();
	}

	init() { 
		// Register event handlers
		this.textbox.onkeydown = event => {
			event = event ? event : window.event;
			this.handleKeyPress();
		};

		// Default layer
		this.layer.style.visibilility = 'hidden';
	}

	fillSuggestions(suggestions) {
		// Clear contents
		this.layer.innerHTML = '';

		// Append new contents
		for (let i = 0; i < suggestions.length; i++) {
			let result = document.createElement('div');
			result.appendChild(document.createTextNode(suggestions[i]));
			this.layer.appendChild(result);
		}

		this.layer.style.visiblity = suggestions.length > 0 ? "visible" : "hidden";
	}

	handleKeyPress() {
		const q = this.textbox.value;
		this.provider.getSuggestions(this, q);
	}
}


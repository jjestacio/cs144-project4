<%@ page import="java.util.ArrayList" %>
<%@ page import="edu.ucla.cs.cs144.*" %>

<!DOCTYPE html>
<html>
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<style type="text/css"> 
  html { height: 100%; } 
  body { height: 100%; margin: 0px; padding-left: 10px; } 
</style> 
<script type="text/javascript" 
    src="http://maps.google.com/maps/api/js?sensor=false"> 
</script> 
<script type="text/javascript"> 
	  function initialize() { 
	  	geocoder=new google.maps.Geocoder();
	    var latlng = new google.maps.LatLng(<%= request.getAttribute("Latitude")%>,<%= request.getAttribute("Longitude")%>); 
	    var myOptions = { 
	      zoom: 14, 
	      center: latlng, 
	      mapTypeId: google.maps.MapTypeId.ROADMAP 
	    }; 
	    var map = new google.maps.Map(document.getElementById("map_canvas"), 
	        myOptions); 
	    geocoder.geocode({'address': '<%=request.getAttribute("Location")%>, <%=request.getAttribute("Country") %>'}, function(results, status) {
	    	if (status == google.maps.GeocoderStatus.OK) {
	    		map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });	    		
	    	}
	    });
	  } 
	</script>

<link rel="stylesheet" type="text/css" href="autosuggest.css" /> 
<script type="text/javascript">
window.onload = function () {
           var oTextbox = new AutoSuggestControl(document.getElementById("keywordtextbox"), new StateSuggestions()); 
        }
</script>

<body onload="initialize()"> 

<h2> Search Result for Item <%= request.getAttribute("ItemID") %></h2>
<h1><%= request.getAttribute("Name") %></h1>

<h3> <u> Seller Information </u> </h3>
<p><b>Seller ID: </b> <%= request.getAttribute("SellerID")%></p>
<p><b>Seller Rating: </b> <%= request.getAttribute("Seller_Rating")%></p>

<h3> <u> Item Categories </u> </h3>
<% ArrayList<String> Categories = (ArrayList<String>) request.getAttribute("Categories"); %>
<div>
<ul>
		<% for (String item: Categories) { %>
			<li><%= item%></li></a>	
		<% } %>
</ul>
</div>

<h3> <u> Other Information </u> </h3>
<div>
	<p><b>Currently: </b> <%= request.getAttribute("Currently")%></p>
	<p><b>Buy Price: </b> <%= request.getAttribute("Buy_Price")%></p>
	<p><b>Started on: </b> <%= request.getAttribute("Started")%></p>
	<p><b>Ends on: </b> <%= request.getAttribute("Ends")%></p>
	<p><b>Description: </b> <%= request.getAttribute("Description")%></p>
	<p><b>First Bid: </b> <%= request.getAttribute("First_Bid")%></p>
</div>

<h3> <u> Bids </u> </h3>
<p><b>Number of Bids: </b> <%= request.getAttribute("Number_of_Bids")%></p>
<% ArrayList<ItemServlet.Bid> Bids = (ArrayList<ItemServlet.Bid>) request.getAttribute("Bids");
%>
<ul>
	<% for(ItemServlet.Bid bid: Bids) { %>
		<li>
			<p><b>Bidder ID: </b> <%= bid.bidderID%></p>
			<p><b>Bidder Rating: </b> <%= bid.bidderRating%></p>
			<p><b>Location: </b> <%= bid.bidderLocation%></p>
			<p><b>Country: </b> <%= bid.bidderCountry%></p>
			<p><b>Time: </b> <%= bid.bidTime%></p>
			<p><b>Amount: </b> <%= bid.bidAmount%></p>
		</li>
		<% } %>
</ul>

<h3> <u> Location </u> </h3>
<div>
	<p><b>Location: </b> <%= request.getAttribute("Location")%></p>
	<p><b>Country: </b> <%= request.getAttribute("Country")%></p>

	<% if (request.getAttribute("Latitude") != "" || request.getAttribute("Longitude") != "") { %>
		<p><b>Latitude: </b> <%= request.getAttribute("Latitude")%></p>
		<p><b>Longitude: </b> <%= request.getAttribute("Longitude")%></p>
	<% } %>
</div>

<% if (request.getAttribute("Latitude") != "" || request.getAttribute("Longitude") != "") { %>
	<div id="map_canvas" style="width:50%; height:50%"></div> 
<% } %>

</body> 
</html>
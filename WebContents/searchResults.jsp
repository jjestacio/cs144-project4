<%@ page import="edu.ucla.cs.cs144.SearchResult" %>

<!DOCTYPE html>
<html>
<head>
<title>CS 144 Project 4</title>
</head>
<link rel="stylesheet" href="styles.css"><link>
<h1 style="text-align:center">Project 4</h1>

<form name = "query" action = "./search" method="GET" action="" style="text-align:center"> 
<h2 style="text-align:center">Item Search</h2>
    <div>
		<input id="queryBox" type="text" name="q" required>
		<button type="submit" name="submit">Submit</button>
    </div>
    <input name="numResultsToSkip" value="0" type="hidden">
	<input name="numResultsToReturn" value="20" type="hidden">
</form>

<div id="suggestions"></div>

<% SearchResult[] results = (SearchResult[])request.getAttribute("results"); %>
<div>
	<% if ((String)request.getAttribute("q") != null) { %> 
	<h2>Search Results for '<%= (String)request.getAttribute("q")%>'</h2>	
	<% } %>
	<ul>
		<% for (SearchResult item: results) { %>
			<a href="/eBay/item?id=<%=item.getItemId()%>"> <li><%= item.getItemId() %>: <%= item.getName()%> </li> </a>	
		<% } %>
	</ul>
</div>

<div>
<%
	String q = (String)request.getAttribute("q");
	int numResultsToSkip = Integer.parseInt((String)request.getAttribute("numResultsToSkip"));
	int numResultsToReturn = Integer.parseInt((String)request.getAttribute("numResultsToReturn"));
	int skip = numResultsToSkip - 20;
	if (numResultsToSkip - 20 < 0)
		skip = 0;
 	if (numResultsToSkip > 0) { %>
		<a href="/eBay/search?q=<%= q %>&numResultsToSkip=<%= skip %>&numResultsToReturn=20">Previous</a> 
	<% } %>

	<% if (results.length == numResultsToReturn) { %>
	<a href="/eBay/search?q=<%= q %>&numResultsToSkip=<%= numResultsToSkip+20 %>&numResultsToReturn=20">Next</a>
	<% } %>
</div>

<script src="AutosuggestControl.js"></script>
<script src="SuggestionProvider.js"></script>
<script type="text/javascript">
	window.onload = () => {
		new AutoSuggestControl(
			document.getElementById("queryBox"),
			new SuggestionProvider()
		);
	}
</script>

</html>
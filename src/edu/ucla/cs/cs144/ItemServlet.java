package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.text.SimpleDateFormat;

import java.io.*;
import java.text.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;

import edu.ucla.cs.cs144.AuctionSearch;


public class ItemServlet extends HttpServlet implements Servlet {
       
    public ItemServlet() {}

    public static class Bid {
    	public String item_id;
    	public String bidderID;
    	public String bidTime;
    	public String bidAmount;
    	public String bidderLocation;
    	public String bidderCountry;
    	public String bidderRating;

    	Bid(String item_id, String bidderID, String bidTime, String bidAmount, String bidderLocation, String bidderCountry, String bidderRating) {
    		this.item_id = item_id;
    		this.bidderID = bidderID;
    		this.bidTime = bidTime;
    		this.bidAmount = bidAmount;
    		this.bidderLocation = bidderLocation;
    		this.bidderCountry = bidderCountry;
    		this.bidderRating = bidderRating;
    	}
    }

    static class MyErrorHandler implements ErrorHandler {
        
        public void warning(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void error(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void fatalError(SAXParseException exception)
        throws SAXException {
            exception.printStackTrace();
            System.out.println("There should be no errors " +
                               "in the supplied XML files.");
            System.exit(3);
        }
        
    }
    
    /* Non-recursive (NR) version of Node.getElementsByTagName(...)
     */
    static Element[] getElementsByTagNameNR(Element e, String tagName) {
        Vector< Element > elements = new Vector< Element >();
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
            {
                elements.add( (Element)child );
            }
            child = child.getNextSibling();
        }
        Element[] result = new Element[elements.size()];
        elements.copyInto(result);
        return result;
    }
    
    /* Returns the first subelement of e matching the given tagName, or
     * null if one does not exist. NR means Non-Recursive.
     */
    static Element getElementByTagNameNR(Element e, String tagName) {
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
                return (Element) child;
            child = child.getNextSibling();
        }
        return null;
    }
    
    /* Returns the text associated with the given element (which must have
     * type #PCDATA) as child, or "" if it contains no text.
     */
    static String getElementText(Element e) {
        if (e.getChildNodes().getLength() == 1) {
            Text elementText = (Text) e.getFirstChild();
            return elementText.getNodeValue();
        }
        else
            return "";
    }
    
    /* Returns the text (#PCDATA) associated with the first subelement X
     * of e with the given tagName. If no such X exists or X contains no
     * text, "" is returned. NR means Non-Recursive.
     */
    static String getElementTextByTagNameNR(Element e, String tagName) {
        Element elem = getElementByTagNameNR(e, tagName);
        if (elem != null)
            return getElementText(elem);
        else
            return "";
    }
    
    /* Returns the amount (in XXXXX.xx format) denoted by a money-string
     * like $3,453.23. Returns the input if the input is an empty string.
     */
    static String strip(String money) {
        if (money.equals(""))
            return money;
        else {
            double am = 0.0;
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            try { am = nf.parse(money).doubleValue(); }
            catch (ParseException e) {
                System.out.println("This method should work for all " +
                                   "money values you find in our data.");
                System.exit(20);
            }
            nf.setGroupingUsed(false);
            return nf.format(am).substring(1);
        }
    }

    static String convertDateTimeFormat(String XML_DateTime) {
    	SimpleDateFormat inputFormat = new SimpleDateFormat("MMM-dd-yy HH:mm:ss");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date;
		String newFormat = "";

		try {
			date = inputFormat.parse(XML_DateTime);
			newFormat = outputFormat.format(date);
		} catch(ParseException e) {
			System.out.println("ParseException error.");
		}

		return newFormat;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String itemIDVal = request.getParameter("id");
        int itemIDInt;
        boolean isString = false;

        try {
        	itemIDInt = Integer.parseInt(itemIDVal);
        } catch(NumberFormatException e) {
        	isString = true;
        	request.setAttribute("ItemID", itemIDVal);
        	request.setAttribute("Name", "");
        	request.setAttribute("Categories", "");
        	request.setAttribute("Country", "");
        	request.setAttribute("Currently", "");
        	request.setAttribute("Buy_Price", "");
        	request.setAttribute("Started", "");
        	request.setAttribute("Ends", "");
        	request.setAttribute("Description", "");
        	request.setAttribute("First_Bid", "");
        	request.setAttribute("Number_of_Bids", "");
        	request.setAttribute("Bids", "");
        	request.setAttribute("Location", "");
        	request.setAttribute("Latitude", "");
        	request.setAttribute("Longitude", "");
        	request.setAttribute("SellerID", "");
        	request.setAttribute("Seller_Rating", "");
        }

        	if (isString == false) {
        		String xmlData = AuctionSearch.getXMLDataForItemId(itemIDVal);
        		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        		DocumentBuilder documentBuilder = null;

        		try {
        			documentBuilder = documentFactory.newDocumentBuilder();
        		} catch(ParserConfigurationException e) {
        			System.out.println("Parser configuration exception.");
        		}

        		if (xmlData != "") {
        			InputSource inputSource = new InputSource(new StringReader(xmlData));
        			Document document = null;

        			if (documentBuilder == null) {
        				System.out.println("DocumentBuilder is null.");
        				return;
        			}

        			try {
        				document = documentBuilder.parse(inputSource);
        			} catch (IOException e) {
           	 			e.printStackTrace();
            			System.exit(3);
			        } catch (SAXException e) {
			            System.out.println("Parsing error on file " + xmlData);
			            System.out.println("  (not supposed to happen with supplied XML files)");
			            e.printStackTrace();
			            System.exit(3);
			        }

			        Element root = document.getDocumentElement();

			        if (root != null) {
			        	String itemID = root.getAttribute("ItemID");
			            String name = getElementTextByTagNameNR(root, "Name");
			            String currently = strip(getElementTextByTagNameNR(root, "Currently"));
			            String buy_price = strip(getElementTextByTagNameNR(root, "Buy_Price"));
			            String first_bid = strip(getElementTextByTagNameNR(root, "First_Bid"));
			            String number_of_bids = getElementTextByTagNameNR(root, "Number_of_Bids");
			            String location = getElementTextByTagNameNR(root, "Location");
			            String country = getElementTextByTagNameNR(root, "Country");

			            Element locationElement = getElementByTagNameNR(root, "Location");
			            String latitude = locationElement.getAttribute("Latitude");
			            String longitude = locationElement.getAttribute("Longitude");

			            String started = convertDateTimeFormat(getElementTextByTagNameNR(root, "Started"));
			            String ends = convertDateTimeFormat(getElementTextByTagNameNR(root, "Ends"));

			            Element seller = getElementByTagNameNR(root, "Seller");
            			String sellerID = seller.getAttribute("UserID");
            			String sellerRating = seller.getAttribute("Rating");

            			String description = getElementTextByTagNameNR(root, "Description");

            			Element parent_bids = getElementByTagNameNR(root, "Bids");
            			Element[] bids = getElementsByTagNameNR(parent_bids, "Bid");
            			ArrayList<Bid> bidList = new ArrayList();

            			for (int i = bids.length - 1; i >= 0; i--) {
            				Element bid = bids[i];
            				Element bidder = getElementByTagNameNR(bid, "Bidder");
                			String bidderID = bidder.getAttribute("UserID");
                			String bidderRating = bidder.getAttribute("Rating");
                			String bidderLocation = getElementTextByTagNameNR(bidder, "Location");
                			String bidderCountry = getElementTextByTagNameNR(bidder, "Country");

                			String time = convertDateTimeFormat(getElementTextByTagNameNR(bid, "Time"));

                			String amount = "$" + strip(getElementTextByTagNameNR(bid, "Amount"));

                			Bid tempBid = new Bid(itemID, bidderID, time, amount, bidderLocation, bidderCountry, bidderRating);
                			bidList.add(tempBid);
            			}

            			Element[] categories = getElementsByTagNameNR(root, "Category");
            			ArrayList<String> categoryList = new ArrayList<String>();
            			for (Element category : categories) {
			                String category_string = getElementText(category);
			                categoryList.add(category_string);
			            }

			            request.setAttribute("ItemID", itemID);
			            request.setAttribute("Name", name);
			            request.setAttribute("Categories", categoryList);

			            if (country != "") {
			            	request.setAttribute("Country", country);
			            }
			            else {
			            	request.setAttribute("Country", "");
			            }

			            request.setAttribute("Currently", "$" + currently);

			            if (buy_price != "") {
			            	request.setAttribute("Buy_Price", "$" + buy_price);
			            }
			            else {
			            	request.setAttribute("Buy_Price", "N/A");
			            }

			            request.setAttribute("Started", started);
			            request.setAttribute("Ends", ends);
			            request.setAttribute("Description", description);
			            request.setAttribute("First_Bid", "$" + first_bid);

			            if (number_of_bids == "" && bids.length == 0) {
			            	request.setAttribute("Number_of_Bids", "0");
			            }
			            else {
			            	request.setAttribute("Number_of_Bids", bids.length);
			            }

			            request.setAttribute("Bids", bidList);
			            
			            if (location == "") {
			            	request.setAttribute("Location", "");
			            }
			            else {
			            	request.setAttribute("Location", location);
			            }

			            if (latitude == "") {
			            	request.setAttribute("Latitude", "");
			            }
			            else {
			            	request.setAttribute("Latitude", latitude);
			            }

			            if (longitude == "") {
			            	request.setAttribute("Longitude", "");
			            }
			            else {
			            	request.setAttribute("Longitude", longitude);
			            }

			            request.setAttribute("SellerID", sellerID);
			            request.setAttribute("Seller_Rating", sellerRating);
			        }
			        else {
			        	request.setAttribute("ItemID", itemIDVal);
			        	request.setAttribute("Name", "");
			        	request.setAttribute("Categories", "");
			        	request.setAttribute("Country", "");
			        	request.setAttribute("Currently", "");
			        	request.setAttribute("Buy_Price", "");
			        	request.setAttribute("Started", "");
			        	request.setAttribute("Ends", "");
			        	request.setAttribute("Description", "");
			        	request.setAttribute("First_Bid", "");
			        	request.setAttribute("Number_of_Bids", "");
			        	request.setAttribute("Bids", "");
			        	request.setAttribute("Location", "");
			        	request.setAttribute("Latitude", "");
			        	request.setAttribute("Longitude", "");
			        	request.setAttribute("SellerID", "");
			        	request.setAttribute("Seller_Rating", "");
			        }
        		}
        		else {
        			request.setAttribute("ItemID", itemIDVal);
		        	request.setAttribute("Name", "");
		        	request.setAttribute("Categories", "");
		        	request.setAttribute("Country", "");
		        	request.setAttribute("Currently", "");
		        	request.setAttribute("Buy_Price", "");
		        	request.setAttribute("Started", "");
		        	request.setAttribute("Ends", "");
		        	request.setAttribute("Description", "");
		        	request.setAttribute("First_Bid", "");
		        	request.setAttribute("Number_of_Bids", "");
		        	request.setAttribute("Bids", "");
		        	request.setAttribute("Location", "");
		        	request.setAttribute("Latitude", "");
		        	request.setAttribute("Longitude", "");
		        	request.setAttribute("SellerID", "");
		        	request.setAttribute("Seller_Rating", "");
        		}
        	}
        request.getRequestDispatcher("/itemResults.jsp").forward(request, response);
    }
}

package edu.ucla.cs.cs144;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProxyServlet extends HttpServlet implements Servlet {
       
    public ProxyServlet() {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String q = request.getParameter("q");

        if (q == null || q.isEmpty())
        	q = " "; // Google API returns error for ""

        try {
        	// Create connection
        	URL url = new URL("http://google.com/complete/search?output=toolbar&q=" + q);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("\nError with status code: " + con.getResponseCode() + "\n");
			
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
				String output = "";
				String line = "";

				while ((line = br.readLine()) != null)
					output += line;

				PrintWriter out = response.getWriter();
				out.println(output);

				br.close();
			}

        } catch (MalformedURLException e) {
        	System.out.println(e);
        } catch (IOException e) {
        	System.out.println(e);
        }

        response.setContentType("text/xml");
    }
}

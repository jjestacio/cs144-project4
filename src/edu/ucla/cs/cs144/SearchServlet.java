package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucla.cs.cs144.AuctionSearch;
import edu.ucla.cs.cs144.SearchResult;

public class SearchServlet extends HttpServlet implements Servlet {
       
    public SearchServlet() {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String q = request.getParameter("q");
        String numResultsToSkipString = request.getParameter("numResultsToSkip");
        String numResultsToReturnString = request.getParameter("numResultsToReturn");

        int numResultsToSkip = 0;
        int numResultsToReturn = 20;
        
        if (numResultsToSkipString != "") {
        	try {
        		numResultsToSkip = Integer.parseInt(numResultsToSkipString);
        	} catch(NumberFormatException e) {
        		numResultsToSkip = 0;
        		numResultsToSkipString = "0";
        	}
        }

        if (numResultsToReturnString != "") {
        	try {
        		numResultsToReturn = Integer.parseInt(numResultsToReturnString);
        	} catch(NumberFormatException e) {
        		numResultsToReturn = 20;
        		numResultsToReturnString = "20";
        	}
        }

        AuctionSearch auctionSearch = new AuctionSearch();
        SearchResult[] results = auctionSearch.basicSearch(q, numResultsToSkip, numResultsToReturn);

        request.setAttribute("results", results);
        request.setAttribute("q", q);
        request.setAttribute("numResultsToSkip", numResultsToSkipString);
        request.setAttribute("numResultsToReturn", numResultsToReturnString);
        request.getRequestDispatcher("/searchResults.jsp").forward(request, response);
    }
}
